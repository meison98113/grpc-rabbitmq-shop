import amqplib from "amqplib";
import { Options } from "amqplib/properties";
import { pino } from "pino";

import { Logger } from "../logger";

export type RMQConnection = {
  connection: amqplib.Connection;
  logger: pino.Logger;
  prefix?: string;
  postfix?: string;
  defaultPublishOptions: Options.Publish;
  defaultConsumeOptions: Options.Consume;
  defaultAssertExchangeOptions: Options.AssertExchange;
  defaultAssertQueueOptions: Options.AssertQueue;
};

export type RabbitMqClientConfig = {
  host: string;
  port: number;
  username: string;
  password: string;
  heartbeat?: number;
  vhost?: string;
  prefix?: string;
  postfix?: string;
};

const initConnection = async (
  logger: pino.Logger,
  config: RabbitMqClientConfig,
  onConnectCallback?: () => any
) => {
  const connectionOptions: Options.Connect = {
    hostname: config.host,
    port: config.port,
    username: config.username,
    password: config.password,
    heartbeat: config.heartbeat || 10,
  };

  if (config.vhost) {
    connectionOptions.vhost = config.vhost;
  }

  logger.info(
    `try to connection to rabbit host: ${connectionOptions.hostname}, port: ${connectionOptions.port}`,
    true
  );

  const connection = await amqplib.connect(connectionOptions);

  connection.on("error", (err) => {
    logger.error("connection on error callback: %o", err);
  });

  logger.info(`connected`);

  if (onConnectCallback) {
    logger.info("_onConnectCallback running...");

    try {
      await onConnectCallback();
    } catch (e) {
      logger.error(e, "error in _onConnectCallback");
      throw e;
    }

    logger.info("_onConnectCallback finished success");
  }

  return connection;
};

export const RMQConnection = {
  new: async (options: {
    logger: pino.Logger;
    config: RabbitMqClientConfig;
    reconnect?: boolean;
    defaultPublishOptions?: Options.Publish;
    defaultConsumeOptions?: Options.Consume;
    defaultAssertExchangeOptions?: Options.AssertExchange;
    defaultAssertQueueOptions?: Options.AssertQueue;
    onConnectCallback?: () => any;
  }): Promise<RMQConnection> => {
    const {
      logger: propsLogger,
      config,
      reconnect = true,
      defaultPublishOptions = {},
      defaultConsumeOptions = {},
      defaultAssertExchangeOptions = {},
      defaultAssertQueueOptions = {},
      onConnectCallback,
    } = options;

    const logger = Logger.addFnStack(propsLogger, `RabbitMQ`);

    const connection = await initConnection(logger, config, onConnectCallback);

    const rmq = {
      connection,
      logger,
      defaultPublishOptions,
      defaultConsumeOptions,
      defaultAssertExchangeOptions,
      defaultAssertQueueOptions,
      prefix: config.prefix,
      postfix: config.postfix,
    };

    if (reconnect) {
      connection.on("close", () => {
        logger.error("reconnecting");

        // . Create new connection on close and repeat forever
        return setTimeout(async () => {
          rmq.connection = await initConnection(
            logger,
            config,
            onConnectCallback
          );
        }, 1000);
      });
    }

    return rmq;
  },
};

