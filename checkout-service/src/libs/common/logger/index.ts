import { pino } from "pino";

export let pinoLogger: pino.Logger = {
  info: (...args: any[]) => {
    // eslint-disable-next-line no-restricted-syntax
    console.log("PINO LOGGER NOT INITIATED YET INFO", args);
  },
  error: (...args: any[]) => {
    // eslint-disable-next-line no-restricted-syntax
    console.error("PINO LOGGER NOT INITIATED YET ERROR", args);
  },
} as unknown as pino.Logger;

export const Logger = {
  init: (mixin: () => Record<any, any>): pino.Logger => {
    pinoLogger = pino({
      mixin,
    });
    pinoLogger.info = pinoLogger.info.bind(pinoLogger);
    pinoLogger.debug = pinoLogger.debug.bind(pinoLogger);
    pinoLogger.warn = pinoLogger.warn.bind(pinoLogger);
    pinoLogger.error = pinoLogger.error.bind(pinoLogger);

    return pinoLogger;
  },
  addFnStack: (logger: pino.Logger, fnName: string): pino.Logger => {
    const fnStack: string[] | undefined = logger.bindings()["fnStack"];

    return logger.child({
      fnStack: fnStack ? [...fnStack, fnName] : [fnName],
    });
  },
  logRethrow: async <R>(
    logger: pino.Logger,
    fn: () => Promise<R>
  ): Promise<R> => {
    try {
      return await fn();
    } catch (e) {
      logger.error(e);
      throw e;
    }
  },
};
