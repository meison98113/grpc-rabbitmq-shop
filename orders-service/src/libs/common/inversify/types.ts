import { QueryArrayResult } from 'pg';

export const DB = Symbol.for('DB');
export interface DBInterface {
  connect(): Promise<void | Error>,
  query(str: string, params: (string | number | boolean | null | undefined)[]): Promise<QueryArrayResult | Error>,
  disconnect(): Promise<void | Error>
}