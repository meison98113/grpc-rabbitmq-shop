import { Container } from "inversify";
import * as TYPES from './types';
import { PostgresDB } from '../postgresql';

const serviceContainer = new Container();

serviceContainer.bind<TYPES.DBInterface>(TYPES.DB).to(PostgresDB).inSingletonScope();

export { serviceContainer };