import * as dotenv from 'dotenv';
const protoLoader = require("@grpc/proto-loader");
const path = require("path");

const grpc = require("@grpc/grpc-js");
const PROTO_PATH = path.resolve(__dirname, "../proto/orders.proto");

dotenv.config();

const options = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
};

const packageDefinition = protoLoader.loadSync(PROTO_PATH, options);
const { OrdersService } = grpc.loadPackageDefinition(packageDefinition);

export const client = new OrdersService(
  process.env.RPC_ENDPOINT,
  grpc.credentials.createInsecure()
);