const protoLoader = require("@grpc/proto-loader");
const grpc = require("@grpc/grpc-js");
const path = require("path");
const PROTO_PATH = path.resolve(__dirname, "..libs/common/proto/orders.proto");

const options = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
};

const packageDefinition = protoLoader.loadSync(PROTO_PATH, options);
const ordersProto = grpc.loadPackageDefinition(packageDefinition);

server.addService(ordersProto.OrdersService.service, {
  getAllOrders: (_: any, callback: any) => {
    callback(null, orders);
  },
});



