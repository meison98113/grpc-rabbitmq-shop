import * as dotenv from 'dotenv';
import { serviceContainer } from './libs/common/inversify';
import { DB, DBInterface } from "./libs/common/inversify/types";
import { GRPCServer } from './libs/common/grpc';
import {connectServices} from "./services";
const grpc = require("@grpc/grpc-js");

(async function main() {
  try {
    dotenv.config();
    const { RPC_ENDPOINT } = process.env;
    const DBInstance = serviceContainer.get<DBInterface>(DB);
    const server = new GRPCServer();
    await DBInstance.connect();

    server.bindAsync(
      RPC_ENDPOINT,
      grpc.ServerCredentials.createInsecure(),
      (error: Error | null, port: number) => {
        console.log(`Server running at: ${RPC_ENDPOINT} on ${port} port`);
        process.on('uncaughtException', function ( err: Error ) {
          console.error(
            `Error type: ${ err.name }
            Error message: ${ err.message }
            Error trace: ${ err.stack }`
          );
        });
        process.on('unhandledRejection', function ( reasonAny: any, _p: Promise<any> ) {
          console.error(
            `Error type: Promise unhandled
              Reject message: ${ reasonAny }
          `
          );
        });
        server.start();
        connectServices(server);
      }
    );
  }
  catch (error) {
    console.error(error);
  }
}())