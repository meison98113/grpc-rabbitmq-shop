import { Client } from 'pg';
import { injectable } from 'inversify';
import 'reflect-metadata';
import {DBInterface} from "../inversify/types";

@injectable()
class PostgresDB implements DBInterface {
    private client: Client;
    constructor() {
        const { PG_HOST, PG_PORT, PG_DATABASE, PG_USERNAME, PG_PASSWORD } = process.env;
        const dbOptions = {
            host: PG_HOST,
            port: Number( PG_PORT ),
            database: PG_DATABASE,
            user: PG_USERNAME,
            password: PG_PASSWORD,
            connectionTimeoutMillis: 5000
        };
        this.client = new Client(dbOptions);
        this.client.on('error', (err) => {
            console.log("PostgresDB: error");
            console.log(`PostgresDB crashedAt:- ${new Date().getTime()}`);
            setTimeout(async () => {
                console.log(`PostgresDB reconnect start:- ${new Date().getTime()}`);
                this.client = new Client(dbOptions);
                await this.connect();
                console.log(`PostgresDB reconnect finished successfully:- ${new Date().getTime()}`);
            }, 10000);
        });
    }

    async connect() {
        try {
            return await this.client.connect();
        }
        catch ( err ){
            console.error( err.message || "DB connection error" + JSON.stringify( err ) );
            throw new Error( "Internal error happened" );
        }
    }

    async disconnect() {
        try {
            console.log("Start disconnecting from DB");
            await this.client.end();
            console.log("Disconnection from DB successfully finished");
        }
        catch  ( err ) {
            console.error( err.message || "DB disconnection error" + JSON.stringify( err ) )
            throw new Error( "Internal error happened" );
        }
    }

    async query( queryStr: string, params: any[] ) {
        try {
            console.log("DB query: " + queryStr );
            if( params )
                return await this.client.query( queryStr, params );

            return await this.client.query(queryStr);
        }
        catch ( err ) {
            console.error( err.message || "DB query error" + JSON.stringify( err ) );
            throw new Error( "Internal error happened" );
        }
    }
}

export { PostgresDB };