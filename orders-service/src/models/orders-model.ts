import { Branded } from "../libs/common/types";

type OrderId = Branded<string, 'OrderId'>;
type ProductId = Branded<string, 'ProductId'>;
type OrderProduct = { orderId: OrderId, productId: ProductId, count?: number };
type Order = {
  id: OrderId;
  orderProducts: OrderProduct[]
}

const orders: Order[] = [
  {
    id: "1" as OrderId,
    orderProducts: [
      {
        orderId: '1' as OrderId,
        productId: 'qqq' as ProductId,
        count: 4
      },
      {
        orderId: '1' as OrderId,
        productId: 'www' as ProductId,
        count: 2
      },
    ],
  },
  {
    id: "2" as OrderId,
    orderProducts: [
      {
        orderId: '2' as OrderId,
        productId: 'kdkd' as ProductId,
        count: 3
      },
      {
        orderId: '2' as OrderId,
        productId: 'oss' as ProductId,
        count: 1
      },
    ],
  },
];

export const getAllOrders = (): Order[] | Error => {
  return orders;
}